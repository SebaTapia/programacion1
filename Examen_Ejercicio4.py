#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from random import randint

import os
import time

def clear_screen(): #Funcion dada por el Profesor para borrar Pantalla.

    os.system('cls' if os.name =='nt' else 'clear')

def concatenacion_listas(suma_listas): #Funcion que se encarga de concatenar.

    nueva_lista = [] #Creacion de una nueva lista.

    for i in range(len(suma_listas)): #Recorrimos el largo de la listas a concatenar.
        nueva_lista = nueva_lista + suma_listas[i] #Sumamos cada lista con sus elementos.
        lista_ordenada = sorted(nueva_lista) #Metodo para ordenar una lista.

    return lista_ordenada

def eliminar_repetidos(elementos): #Funcion que se encarga de eliminar repetidos.

    elementos_repetidos = []
    lista_filtrada = [] #En esta lista se almacenara los no repetidos.

    #Metodo para eliminar elementos repetidos
    filtrado = filter(lambda x : x not in lista_filtrada, elementos)
    lista_filtrada.extend(filtrado) #Se agregara solo los elementos no repetidos.

    return lista_filtrada #Se retorna la lista ya filtrada.


def menu():

        #Creacion de nuestras 3 listas a procesar.

        lista_a = []
        lista_b = []
        lista_c = []

        cantidad_elementos = int(input(' Ingrese Cantidad Numeros Por Cada Lista: '))

        for i in range(cantidad_elementos):

            elemento = int(input(' Ingrese Numero de Lista1: '))
            lista_a.append(elemento)
            elemento2 = int(input(' Ingrese Numero de  Lista2: '))
            lista_b.append(elemento2)
            elemento3 = int(input(' Ingrese Numero de Lista3: '))
            lista_c.append(elemento3)

        time.sleep(2)
        clear_screen()

        suma_listas = [lista_a + lista_b + lista_c] #Metodo simple de concatenar.

        lista_principal = concatenacion_listas(suma_listas) #Llamado de la funcion concatenacion.
        lista_limpia = eliminar_repetidos(lista_principal) #Llamado de la funcion limpieza.

        print(' Sublista 1: ', lista_a)
        print(' Sublista 2: ', lista_b)
        print(' Sublista 3: ', lista_c)
        print(' Suma Listas: ', lista_principal)
        print(' Lista Filtrada: ', lista_limpia)

        for i in range(len(lista_limpia)): #Recorremos la lista ya terminada

        #Este bloque de ciclo se analizan tres situaciones.
        #1. Elemento de la lista si es par, se modifica.
        #2. Si el elemento es termina en 0, no se modifica.
        #3. O si el numero en si es igual a cero.

            if (lista_limpia[i] % 2 == 0):
                if (lista_limpia[i] % 10 != 0):
                    if (lista_limpia[i] != 0):
                        #Si se cumplen las tres condiciones  el elemento se cambia.
                        lista_limpia[i] = randint(1,100)

        print(' Lista Final: ', lista_limpia)



menu()
