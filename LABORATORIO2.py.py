#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json
import csv
import os
import time

BorrarPantalla = lambda : os.system('cls') # Funcion para Borrar Pantalla

def cargar_inventario():

    with open('top50.csv') as file:
        data_csv =  csv.reader(file)

        for linea in data_csv:
            print(linea)
    return linea

def top_artista():

    top_artista = []
    top_artista_filtrado = []
    el_mismisimo = 0

    with open('top50.csv') as file:
        data_csv =  csv.reader(file)
        next(data_csv) # Evita la primera linea

        for linea in data_csv:
            top_artista.append(linea[2])

        #Metodo lambda para eliminar las repeticiones de los Artistas
        b = filter(lambda x : x not in top_artista_filtrado, top_artista)
        top_artista_filtrado.extend(b)
        print(' EL top de Artista es: \n', top_artista_filtrado)


        #Metodo para contar los artistas mas repetidos en la Lista Original
        for artista_mayor in top_artista:
            if top_artista.count(artista_mayor) > 1:
                el_mismisimo = artista_mayor
        print(' El artista mas escuchado fue: ', el_mismisimo)

def funcion_zumba():

    bailongo = []

    with open('top50.csv') as file:
        data_csv =  csv.reader(file)
        next(data_csv) # Evita la primera linea

        for linea in data_csv:
            bailongo.append(linea[6])

        for i in range(len(bailongo)):
            bailable = max(bailongo)
            no_bailable = min(bailongo)

        print('')
        print(' La cancion mas bailable tiene un ritmo de {0} y la menos bailable tiene un ritmo de {1} '.format(bailable,no_bailable))

def funcion_popularidad():

    popularidad = []

    with open('top50.csv') as file:
        data_csv =  csv.reader(file)
        next(data_csv) # Evita la primera linea

        for linea in data_csv:
            popularidad.append(linea[13])
        cancion_popular = max(popularidad)
        print(' La cancion mas popular es de Billie English, tiene {0}% de popularidad '.format(cancion_popular))

def menu():
    while True:
        print(' I: Inventario ')
        print(' A: Top 50 Artistas ')
        print(' B: Top canciones Bailables ')
        print(' P: Cancion Hit del Verano ')
        print(' S: Salir del Menu ')

        print()

        opcion = str(input(' Ingrese una opcion: '))

        if (opcion.upper() == 'I'):
            cargar_inventario()
            enter = str(input(' Presione Enter para avanzar: '))
            if ( enter == ''):
                pass
        elif (opcion.upper() == 'A'):
            top_artista()
            enter = str(input(' Presione Enter para avanzar: '))
            if ( enter == ''):
                pass
        elif (opcion.upper() == 'B'):
            funcion_zumba()
            print('')
            enter = str(input(' Presione Enter para avanzar: '))
            if ( enter == ''):
                pass
        elif ( opcion.upper() == 'P'):
            funcion_popularidad()

        elif (opcion.upper() == 'S'):
            break

        else:
            print(' Opcion No Valida ')
            time.sleep(1.5)
            BorrarPantalla()


if __name__ == '__main__':
    menu()
