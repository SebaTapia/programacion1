#!/usr/win/env_python3
#! -*- coding:utf-8 -*-

import os
import random as r
import time

tamaño_matriz = 6
matriz_conway = []

print ("                             ------------------------------------------------")
print ("                              Bienvenidos al Juego de la vida de John  Conway")
print ("                             ------------------------------------------------")
print ("                                       -------------------------")
print ("                                              ------------")
print ("                                                  -----")
print ("                                                    --")
print('')
print('')

def main(): #Main el cual crea la matriz principal
	for i in range(tamaño_matriz):
		matriz_conway.append([])
		for j in range(tamaño_matriz):
			azar = r.randint(0,1) #El llenado es aleatorio, solo 0 y 1 
			matriz_conway[i].append(azar)

	for i in range(tamaño_matriz): #Lectura de matriz tanto en I y J
		for j in range(tamaño_matriz):
			print (matriz_conway[i][j],end='')#Aca se imprimie matriz
		print('')
	return matriz_conway #Devolucion de matriz, en la cual se iguala matriz_buena = main()
		
def main_contador(matriz_buena): #Funcion de contadores de celulas vivas y muertas en el origen
	contador_vivas = 0
	contador_muertas = 0
	for i in range(len(matriz_buena)):               
		for j in range(len(matriz_buena)):
			if ( matriz_buena[i][j] == 1 ): # Si cumple el requisito de posicion I y J == 1 
				contador_vivas = contador_vivas  + 1 # ----> Se añade una celula con vida
			else:
				contador_muertas = contador_muertas + 1
		
	return contador_vivas, contador_muertas # Retorno de los contadores
	
def main_vecinos(matriz_buena): # Funcion para ver los 8 vecinos de cada celula
	for f in range(len(matriz_buena)): 
		for c in range(len(matriz_buena)):
			vecino = 0
			if (matriz_buena[f][c-1] == 1): #Vecino Izquierda
				vecino = vecino + 1
			if (matriz_buena[f][c+1] == 1): #Vecino  Derecha
				vecino = vecino + 1
			if (matriz_buena[f-1][c] == 1): #Vecino Arriba
				vecino = vecino + 1
			if (matriz_buena[f+1][c] == 1): #Vecino Abajo
				vecino = vecino + 1
			if (matriz_buena [f-1][c-1] == 1): #Vecino Arriba Izquierda
				vecino = vecino  + 1
			if (matriz_buena[f-1][c+1] == 1): #Vecino  Arriba Derecha
				vecino = vecino + 1
			if (matriz_buena [f+1][c-1] == 1): #Vecino Abajo Izquierda
				vecino = vecino  + 1
			if (matriz_buena[f+1][c+1] == 1): #Vecino Abajo Derecha
				vecino = vecino + 1
			main_game(f,c,vecino,matriz_buena)
			return vecino

def main_game(f,c,contador_vecino,matriz_buena):
	if (matriz_buena[f][c] == 1):
		if ( contador_vecino <= 1):
			matriz_buena [f][c] = 0 
		if ((contador_vecino >= 2) and (contador_vecino <= 3)):
			matriz_buena[f][c]= 1
		if ( contador_vecino >= 4):
			matriz_buena[f][c] = 0 
	if ( matriz_buena[f][c] == 1):
		if ( contador_vecino  == 3):
			matriz_buena[f][c] = 1
		else:
			matriz_buena[f][c] = 0
	return matriz_buena

def main_final(matriz_buena, reglas):
	for i in range(len(matriz_buena)):
		for j in range(len(matriz_buena)):
			print (matriz_buena[i][j], end='')
		print('')
	return matriz_buena
			
		
		
print(" ------------------------------------------------")
matriz_buena = main() # Igualacion del main principal, para posterior uso de ella
print(" ------------------------------------------------")
print ("Celulas vivas y Muertas es de: ", main_contador(matriz_buena)) #Llamado de main_contador
contador_vecino = main_vecinos(matriz_buena)
print('')

reglas = main_game

main_final(matriz_buena,reglas)

