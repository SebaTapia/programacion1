#!/usr/win/env_python3
#! -*- coding:utf-8 -*-

import random # LLamado de la funcion random

def function_matriz(): # Funcion donde crea la matriz 15*12
    seba = []
    for i in range(12): # 12 columnas
        seba.append(['']*15) # Como la matriz no es nxn, se multiplica por 15 o quedara fuera de rango
        for j in range(15): #15 filas
            seba[i][j] = random.randint(-10,10) # Metodo dado por el profesor para generar numeros
                                                # aleatorios del intervalo [-10,10]
    return seba

xd = function_matriz() # Asignacion del return de matriz a una variable

def function_print(xd): # funcion en la cual se imprime la funcion

    for i in range(12):
        for j in range(15):
            print( xd[i][j], end = '\t') # end = '\t', metodo para organizar la matriz
        print()

def function_suma(xd):
    suma = []               # Creacion de la lista de las sumas
    contador_suma = 0       # Variable donde almacenara las sumas de los elementos de cada columna

    for i in range(12):  # Recorrido de las 12 columnas
        for j in range(5): # Recorrido de los primeros 5 elementos de las filas
            contador_suma = contador_suma + xd[i][j] # Contador donde se suma el elemento de xd[i][j]
                                                     # y se asigna nuevo valor, hasta 5 ciclos
        suma.append(contador_suma) # Metodo para agregar a la lista de suma
    return suma # Retorno de la lista

sumaaa = function_suma(xd) # Asingancion de variable a la lista

def  function_negativos(xd):
    negativos = []
    for i in range(12):
        for j in range(15):
            if (xd[i][j] < 0): #Primera restriccion que el elemento de i y j sea menor a 0
                if ( j >= 5): # 2da restriccion, donde la columna debe ser mayor o igual a 5
                    if ( j <= 9): #3ra restriccion, donde la columna debe ser menor o igual a 9
                        negativos.append(xd[i][j]) # si cumple las 3 restricciones dadas
                                                       # se agregara a la lista de menores
    return negativos

negativooos = function_negativos(xd)

function_print(xd)
print('\t')
print('La suma de los primeros 5 elementos de cada columna es de: ', sumaaa)
print('El total de negativos de la columna 5 -> 9 son: ', negativooos)
