#!/usr/win/env_python3
#! -*- coding:utf-8 -*-

main_lista = []

def function_main(main_lista): # funcion para llenar la lista de forma for n
    n = int(input(' Cuantos elementos desea ingresar?: '))
    for i in range(n):
        elemento = str(input(' Agrege elemento: ')) # Se repetira n-veces hasta que i sea == n
        main_lista.append(elemento) # Cuando i == n, se termina de llenar la lista
    print(' La lista final es: ', main_lista)

    main_tuple = tuple(main_lista)     # Se inicia proceso de transformacion de lista a tupla

    return main_lista # Se retrona la variable lista, con la cual se trabajara

a = function_main(main_lista) # Asignacion de la funcion a una variable

lista_temporal = [] #Creacion de una lista temporal

def function_ordenar(a,lista_temporal):

    largo = len(a) #Obtencion del largo de la lista

    for i in range(largo): # se hara i-veces hasta que i  == len(de la lista)
        if ( i == 0): # Si el indice de i == 0, el elemento de 0 pasara a ser el ultimo
            lista_temporal.append(a[largo-1]) # Se agrega a la lista temporal
        else: # En caso contrario  se moveran los elementos de forma seguida
            lista_temporal.append(a[i-1])

    print(' La lista modificada es: ', lista_temporal)

print(' La tupla es: ', tuple(a))

function_ordenar(a,lista_temporal)
