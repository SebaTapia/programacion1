#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#En este ejercicio esta incluido el 2 y 3

from random import randrange

import os
import time

secuencia = "gtgggggggtttatgcctttagaacagcagactactgataactccaatcctgggttgaaaatgccaagggcgccagagagccaaacgatgagcgttggaccacaaacgataaaa"

def clear_screen(): #Funcion dada por el Profesor para borrar Pantalla.

    os.system('cls' if os.name =='nt' else 'clear')

def imprimir_cadena(secuencia): #Funcion que imprime la secuencia.

    print('La secuencia amninoacidica es:\n ')
    print( secuencia)
    print('\n')

def funcion_subsecuencia(secuencia): #Funcion para obtener la subcadena.

    largo_secuencia = len(secuencia) #Obtenemos el Largo de nuestra secuencia.

    #Usamos el largo de la secuencia, para asegurar que nuestra
    #Subcadena respete el largo de nuestra secuencia orgininal.

    extremo_i = randrange(largo_secuencia) #Obtenemos en que pocision inicia la subcadena.
    extremo_t = randrange(largo_secuencia) #Obtenemos en que pocision finaliza la subcadena.


    #Con este ciclo sencillo, aseguramos que siempre la subsecuencia.
    #Empezara  con la posicion mayor[inicio] antes que posicion[final].
    #En otras palabras el aminoacido a cortar tendra mayor posicion
    #Que el aminoacido de comienzo.

    if (extremo_i > extremo_t):
        print(' Posicion del Aminoacido Final: ', extremo_i)
        print(' Posicion del Aminoacido Inicio: ', extremo_t)
        print(' Subc-Cadena: ', secuencia[extremo_t : extremo_i])
        print('\n')


    else:
        print(' Posicion del Aminoacido Final: ', extremo_t)
        print(' Posicion del Aminoacido Inicio: ', extremo_i)
        print(' Sub-Cadena: ', secuencia[extremo_i : extremo_t])
        print('\n')

def busqueda_manual(archivo,string):

    patron = string.upper()

    if (string in archivo ): #Analisis del Patron en la secuencia.
        print(' Ese Patron Existe: ', patron)
        print('\n')

    else:
        print(' Ese Patron No Existe')
        print('\n')

def menu(): #Funcion la cual crea el menu

    while True:

        print(' Bienvenido a DNA Cadenas ')
        print(' A: Para Mostrar Una Sub-Cadena de DNA ')
        print(' C: Para Mostrar Cadena de DNA ')
        print(' M: Busqueda Manual de Patrones ')
        print(' S: Para Salir ')
        print('\n')

        opcion_ingresada = str(input(' Ingrese Una Opcion: '))

        if (opcion_ingresada.upper() == 'A'):
            funcion_subsecuencia(secuencia)
            press_enter = str(input(' Press Enter: '))

            if (press_enter == ''):
                clear_screen()
                pass

        elif (opcion_ingresada.upper() == 'C'):
            imprimir_cadena(secuencia)
            press_enter = str(input(' Press Enter: '))

            if (press_enter == ''):
                clear_screen()
                pass

        elif (opcion_ingresada.upper() == 'M'):
            buscar = str(input(' Ingrese Patron: '))
            busqueda_manual(secuencia,buscar)

            press_enter = str(input(' Press Enter: '))

            if (press_enter == ''):
                clear_screen() #LLamado de Borrar Pantalla
                pass

        elif (opcion_ingresada.upper() == 'S'):
            quit()

        else:
            print(' Opcion No Valida....')
            time.sleep(2)
            clear_screen()


if __name__ == '__main__':
    menu()
