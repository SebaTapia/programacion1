#!/usr/win/env_python3
#! -*- coding:utf-8 -*-

import json, random, os, time
lista_j1 = []
lista_j2 = []
letritas_solitarias = []
tablero_juego = []

ruta ='dic.json'
BorrarPantalla = lambda : os.system('cls')
def main_cargar_datos(ruta):#Function que imprime el total del diccionario
    with open(ruta) as file:
        intento = json.load(file)
        for key in intento['letritas']:
            print('\t  Letras: ', key['Letra'])
            print('\t  Puntaje: ', key['Puntaje'])
            print('\t  Cantidad: ', key['Cantidad'])
            print('')
def main_listas_originales(ruta):#Function que separa las letras del diccionario
    with open(ruta) as file:
        archivo = json.load(file)
        for key in archivo['letritas']:
            a = list(key.get('Letra'))
            letritas_solitarias.append(a)
        return letritas_solitarias
def main_llenado1(lista_j1,letritas_solitarias):#Function que agregue letras al jugador1
    for i in range(8):
        r1 = random.choice(letritas_solitarias)
        lista_j1.append(r1)
    return lista_j1
def main_llenado2(lista_j2,letritas_solitarias):#Function que agregue letras al jugador2
    for i in range(8):
        r2 = random.choice(letritas_solitarias)
        lista_j2.append(r2)
    return lista_j2
def main_tablero():#Creacion del tablero de scrabble
    for i in range(15):
        tablero_juego.append([])
        for j in range(15):
            tablero_juego[i].append(' |    | ')
    return tablero_juego
def main_imprimir(main):
    for i in range(len(main)):
        for j in range(len(main)):
            print(main[i][j],end='')
        print('')
    return main
#Igualacion de Functions con variables
#main_general_lista = main_cargar_datos(ruta)
main_lista = main_listas_originales(ruta)
l1 = main_llenado1(lista_j1,letritas_solitarias)
l2 = main_llenado2(lista_j2,letritas_solitarias)
main = main_tablero()
print('\t                            ____________________________________\n')
print('\t                                      WELCOME TO SCRABBLE')
print('\t                            ____________________________________\n')
time.sleep(1.2)
print('\t                  REGLAS: \n' )
time.sleep(1.5)
print('\t -->  Cada jugador recibira 8 letras\n')
print('\t -->  Podra cambiar solo 3 letras al inicio\n')
print('\t -->  Sin trampas o diosito se enoja\n')
time.sleep(2.5)
jugador1 = str(input("\t -->  Ingrese su nombre (Jugador1): "))
jugador2 = str(input("\t -->  Ingrese su nombre (Jugador2): "))
print('')
print("\t --> Letras de " + str(jugador1), ': ', l1)
print("\t --> Letras de " + str(jugador2), ': ', l2)
print('')
time.sleep(2)
v = 0
print('\t    Generando Tablero....')
time.sleep(1.5)
BorrarPantalla()
print('\t                            ____________________________________\n')
print('\t                                          SCRABBLE')
print('\t                            ____________________________________\n')
print('')
print('')
main_imprimir(main)
