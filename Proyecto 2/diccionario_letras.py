#!/usr/win/env_python3
#! -*- coding:utf-8 -*-
import json

dic = {}
dic['letritas'] = []
dic['letritas'].append({'Letra': 'A', 'Puntaje': 1, 'Cantidad': 12})
dic['letritas'].append({'Letra': 'B', 'Puntaje': 3, 'Cantidad': 2})
dic['letritas'].append({'Letra': 'C', 'Puntaje': 3, 'Cantidad': 4})
dic['letritas'].append({'Letra': 'CH', 'Puntaje': 5, 'Cantidad': 1})
dic['letritas'].append({'Letra': 'D', 'Puntaje': 5, 'Cantidad': 5})
dic['letritas'].append({'Letra': 'E', 'Puntaje': 1, 'Cantidad': 12})
dic['letritas'].append({'Letra': 'F', 'Puntaje': 4, 'Cantidad': 12})
dic['letritas'].append({'Letra': 'G', 'Puntaje': 2, 'Cantidad': 2})
dic['letritas'].append({'Letra': 'H', 'Puntaje': 4, 'Cantidad': 2})
dic['letritas'].append({'Letra': 'I', 'Puntaje': 1, 'Cantidad': 6})
dic['letritas'].append({'Letra': 'J', 'Puntaje': 8, 'Cantidad': 1})
dic['letritas'].append({'Letra': 'L', 'Puntaje': 1, 'Cantidad': 4})
dic['letritas'].append({'Letra': 'LL', 'Puntaje': 8, 'Cantidad': 1})
dic['letritas'].append({'Letra': 'M', 'Puntaje': 3, 'Cantidad': 2})
dic['letritas'].append({'Letra': 'N', 'Puntaje': 8, 'Cantidad': 1})
dic['letritas'].append({'Letra': 'Ñ', 'Puntaje': 1, 'Cantidad': 5})
dic['letritas'].append({'Letra': 'O', 'Puntaje': 1, 'Cantidad': 9})
dic['letritas'].append({'Letra': 'P', 'Puntaje': 3, 'Cantidad': 2})
dic['letritas'].append({'Letra': 'Q', 'Puntaje': 5, 'Cantidad': 1})
dic['letritas'].append({'Letra': 'R', 'Puntaje': 1, 'Cantidad': 5})
dic['letritas'].append({'Letra': 'RR', 'Puntaje': 8, 'Cantidad': 1})
dic['letritas'].append({'Letra': 'S', 'Puntaje': 1, 'Cantidad': 6})
dic['letritas'].append({'Letra': 'T', 'Puntaje': 1, 'Cantidad': 4})
dic['letritas'].append({'Letra': 'U', 'Puntaje': 1, 'Cantidad': 5})
dic['letritas'].append({'Letra': 'V', 'Puntaje': 4, 'Cantidad': 1})
dic['letritas'].append({'Letra': 'X', 'Puntaje': 8, 'Cantidad': 1})
dic['letritas'].append({'Letra': 'Y', 'Puntaje': 4, 'Cantidad': 1})
dic['letritas'].append({'Letra': 'Z', 'Puntaje': 10, 'Cantidad': 1})

with open('dic.json','w') as file:
    json.dump(dic,file,indent=2)
