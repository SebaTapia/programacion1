#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os, time

BorrarPantalla = lambda : os.system('cls') # Funcion para Borrar Pantalla

aminoacidos = {} #Creacion del Diccionario

def inicio(aminoacidos):

    cantidad = int(input(' Ingrese Cantidad de Aminoacidos a Ingresar: '))
    time.sleep(1.5)

    for i in range(cantidad):

        print()

        nombre = str(input(' Ingrese Nombre Del Aminoacido {}: '.format(i+1)))

        nombre_final = nombre.capitalize() #Transformacion de la 1ra letra a Mayus

        molecular = str(input(' Ingresa La Fomula Molecular de ' + str(nombre_final) + ': '))

        formula_molecular = molecular.upper() #Transformacion de minusculas a mayusculas

        aminoacidos[nombre_final] = formula_molecular

        time.sleep(1.5)

    aminoacidos2 = {}
    BorrarPantalla()

    for k, v  in aminoacidos.items():   #Metodo dado para recorrer keys y values
                                        #Transformacion de key --> value y value --> key
        aminoacidos2[v] = k #la key nueva sera el valor y viceversa

    print(' Diccionio Normal --> ', aminoacidos)
    print(' Diccionio Cambiado --> ', aminoacidos2)

    return aminoacidos

inicio(aminoacidos)
