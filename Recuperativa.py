#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#La opcion B responde la alternativa C
#La opcion M responde la alternativa F

import json,os,time

BorrarPantalla = lambda : os.system('cls') # Funcion para Borrar Pantalla

def cargar_archivo(): #Funcion dada por el profesor para abrir un archivo json
    with open('iris.json','r') as file:
        data = json.load(file)
    return data

def suma(variable_ingresar): #Funcion que se encarga hacer las sumas

    lista1 = []
    lista2 = []
    lista3 = []

    #Se usa 150 ya que son 150 lineas de archivo y cada 50 lineas
    #Cambia el tipo de especie

    for i in range(150):
        if i <50:
            lista1.append(variable_ingresar[i]) #Variable a Ingresar dependera de la funcion proemdio
        elif i>=50 and i <100:
            lista2.append(variable_ingresar[i])
        else:
            lista3.append(variable_ingresar[i])

    #Calculo basico mediante la suma de todo y la division del largo de la lista
    promedio1 = sum(lista1)/len(lista1)
    promedio2 = sum(lista2)/len(lista2)
    promedio3 = sum(lista3)/len(lista3)

    print(' Setosas', promedio1, ' Versicolor', promedio2,' Viginica',promedio3)

def especies(): #Funcion que se encarga de mostrar las especies

    dic_lista = cargar_archivo() # Funcion que llama a la funcion donde carga el Archivo json
    diccionario = {}

    lista_especies = [] #Lista la cual guardara las especies
    listas_especies_unicas = [] #Lista la cual guardara las espcies filtradas

    for i in range(len(dic_lista)):
        diccionario[i] = dic_lista[i]

    #For que recorre las keys y values del diccionario
    for key,value in diccionario.items():
        #Utilizamos la variable i para recorrer las values del diccionario principal
        for i in value:
            if i == 'species': #En caso de que la i encuentre la value determada se agregara a la lista
                lista_especies.append(value[i])

    #Proceso para limpiar la lista de Especies
    #En este caso solo requerrimos de las 3 especies unicas del Archivo

    a = filter(lambda x : x not in listas_especies_unicas, lista_especies)
    listas_especies_unicas.extend(a)

    time.sleep(2)
    print('')
    for i in listas_especies_unicas:
            print(' Especie:  {0} '.format(i))
    time.sleep(3)
    BorrarPantalla()
    print('')

def Inventario(): #Funcion que muestra el archivo Json en diccionario

    main_data = cargar_archivo()
    temp = {}
    contador = 0

    time.sleep(2)

    for i in range(len(main_data)):
        temp[i] = main_data[i]
        contador = contador + 1 #variable para contar las lineas del archivo
        print(temp[i])

    print('')
    time.sleep(1.5)
    print(' La cantidad de lineas del archivo es de {}'.format(contador))
    print('')
    key = str(input(' Aprete Enter Para Avanzar: '))
    if (key == ''):
        BorrarPantalla()
        pass

def promedio(): # En esta fucion obtenemos el promedio de largo y Ancho

    dic_lista = cargar_archivo()
    diccionario = {}
    contador = 0

    col_petalo_largo = []
    col_petalo_ancho = []

    for i in range(len(dic_lista)):
        diccionario[i] = dic_lista[i]

    #Recorre los diccionario mediante keys y values
    for key,value in diccionario.items():
        #If la condicion i es igual a una key, guardara el valor de esa key
        for i in value:
            if i == 'petalLength':
                col_petalo_largo.append(value[i])
            elif i == 'petalWidth':
                col_petalo_ancho.append(value[i])
    print('')
    time.sleep(1.5)
    print(" Largo de Petalos: ")
    suma(col_petalo_largo)
    print(" Ancho de Petalos: ")
    suma(col_petalo_ancho)
    print('')
    time.sleep(6)
    BorrarPantalla()

def medidia_maxima(variable_ingresar):

    lista1 = []
    lista2 = []
    lista3 = []

    #Se usa 150 ya que son 150 lineas de archivo y cada 50 lineas
    #Cambia el tipo de especie

    for i in range(150):
        if i <50:
            lista1.append(variable_ingresar[i]) #Variable a Ingresar dependera de la funcion proemdio
        elif i>=50 and i <100:
            lista2.append(variable_ingresar[i])
        else:
            lista3.append(variable_ingresar[i])

    #Metodo para obtener el elemento mayor de una lista
    a = max(lista1)
    b = max(lista2)
    c = max(lista3)

    print(' Setosas: ', a,  ' Versicolor', b,' Viginica',c)


def promedio2():

    dic_lista = cargar_archivo()
    diccionario = {}
    contador = 0

    col_petalo_largo = []

    for i in range(len(dic_lista)):
        diccionario[i] = dic_lista[i]

    #Recorre los diccionario mediante keys y values
    for key,value in diccionario.items():
        #If la condicion i es igual a una key, guardara el valor de esa key
        for i in value:
            if i == 'petalLength':
                col_petalo_largo.append(value[i])

    time.sleep(2)
    print('')
    print(' A continuacion se presentaran las medidas maximas de los largos de un sepalo en sus 3 especies  ')
    medidia_maxima(col_petalo_largo)
    time.sleep(4)
    BorrarPantalla()

def menu():

    while True:

        print(' I: Para Mostrar Inventario del Archivo')
        print(' E: Para Mostrar Las Especies del Archivo ')
        print(' T: Para Mostrar Promedio ( Ancho y Largo) de las Flores ')
        print(' M: Para Mostrar Petalo Mas Grande ')
        print(' S: Para Salir')

        opcion_ingresada = str(input(' Ingrese una Opcion: '))

        if(opcion_ingresada.upper() == 'I'):
            Inventario()

        elif(opcion_ingresada.upper() == 'E'):
            especies()

        elif(opcion_ingresada.upper() == 'T'):
            promedio()

        elif(opcion_ingresada.upper() == 'M'):
            promedio2()

        elif(opcion_ingresada.upper()== 'S'):
            quit()

        else:
            print(' Opcion No valida ')
            time.sleep(1.5)
            BorrarPantalla()


if __name__ == '__main__':
    menu()
