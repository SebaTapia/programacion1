#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Las opciones M,S y A son distintas...
# M equivale a salir del programa, pero con el diccionario terminado, editado, etcetera...
# S equivale a salir del programa, de forma brusca sin nada terminado o sin retornar algo
# A equivale a mostar Diccionario post insertar, edicion, eliminacion del Diccionario
# Se recomienda mostar el Diccionario (opcion A) despues de cada insercion, etc...
# Todo se escribe en minusculas, el algoritmo se encarga de poner mayusculas a todo
#

import json, os, time
BorrarPantalla = lambda : os.system('cls') # Funcion para Borrar Pantalla

def guardar(Aminoacidos):
    with open("Aminoacidos.txt", 'w') as file:
        json.dump(Aminoacidos,file)

def cargar():
    with open("Aminoacidos.txt") as file:
        Aminoacidos = json.load(file)
    return Aminoacidos

def insertar_aminoacido(Aminoacidos):
    time.sleep(1)
    BorrarPantalla()
    cantidad_aa = int(input(' Cuantos Aminoacidos Desea Ingresar: ')) # Pide al usuario ingresar una cantidad determinada
    for i in range(cantidad_aa): # Cuando i = cantidad_aa, terminara el ciclo for
        print()
        nombre_aminoacido = str(input(' Ingrese Nombre del Aminoacido N° {}: '.format(i+1))) # Keys del Diccionario
        nombre_final = nombre_aminoacido.capitalize()
        molecular = str(input(' Ingrese Formula Molecular ' + str (nombre_final) + ': ')) # Values del Diccionario
        molecular_upper = molecular.upper()
        Aminoacidos[nombre_final] = molecular_upper # Creacion del diccionario, donde se iguala las Keys / values
                                                    # Esto sera por cada ciclo for, hasta su finalizacion
    print()
    print(' Creando Diccionario...')
    time.sleep(1)
    print(' Diccionario Creado...')
    time.sleep(1.5)
    BorrarPantalla()
    guardar(Aminoacidos) # LLamado de la fucnion, para crear el archivo Json
    return Aminoacidos

def editar_aminoacido(Aminoacidos):
    time.sleep(1)
    BorrarPantalla()
    nombre_aminoacido = str(input(' Ingrese Nombre del Aminoacido a Editar: ')) #Ingresa el Nombre del AA a editar
    nombre_final = nombre_aminoacido.capitalize() #Transformacion de la primera letra en Mayus
    aa_temporal = Aminoacidos.get(nombre_final) #variable para determinar si existe el AA
    print(' Protocolo de Busqueda Activado...')
    time.sleep(2)
    if not aa_temporal: #Si el AA no se encuentra, arrojada estas lineas
        print(' Aminoacido no Encontrado ')
        time.sleep(1.5)
        print(' Error Protocolo de Busqueda 0b004.exe...')
        time.sleep(2)
    else: # En caso contrario pedira informacion
        key_value = int(input(' Desea Modificar la Value (0) o Key (1): ')) #Variable de tipo Key o Value a cambiar
        if( key_value == 0):
            print()
            molecular = str(input(' Ingrese Nueva Formula Molecular de ' + str(nombre_aminoacido) + ': '))
            molecular_upper = molecular.upper()
            Aminoacidos[nombre_final] = molecular_upper #Determina nueva formula al AA
            print(' Agregando Modificacion...')
            time.sleep(1.5)
            print(' Modificacion Completada Exitosamente...')
            time.sleep(2)

        else:
            valor = Aminoacidos.pop(nombre_final) #Borra la key, almacena el valor
            nombre_editado = str(input(' Ingrese Nuevo Nombre: '))
            nombre2 = nombre_editado.capitalize()
            Aminoacidos[nombre2] = valor #Se agrega al Diccionario el nuevo AA con su valor guardado
            time.sleep(1)
            print(' Protocolo de Eliminacion Activado...')
            time.sleep(2)
            print(' Proceso de Eliminacion Completada')
            time.sleep(2)
            print(' Transformacion Completada Existosamente')
            time.sleep(2)
            guardar(Aminoacidos)
    BorrarPantalla()
    return Aminoacidos

def eliminar_aminoacido(Aminoacidos):
    time.sleep(2)
    BorrarPantalla()
    nombre_aminoacido = str(input(' Ingrese Nombre del Aminoacido a Eliminar: '))
    print(' Protocolo de Busqueda Activado...')
    nombre_final = nombre_aminoacido.capitalize()

    try:
        del Aminoacidos[nombre_final] #Borra la Key
        time.sleep(2)
        print(' Proceso de Eliminacion Activada...')
        time.sleep(1.5)
        print(' Eliminacion Completada Exitosamente...')
        time.sleep(1.5)
        guardar(Aminoacidos) # LLamado de la funcion, para crear el archivo Json

    except:
        time.sleep(2)
        print(' Aminoacido No Encontrado... ')
        time.sleep(1.5)
        print(' Error Protocolo de Busqueda 0f005p.exe...')
        time.sleep(2)
    BorrarPantalla()
    return Aminoacidos

def buscar_aminoacido(Aminoacidos):

    time.sleep(1.5)
    BorrarPantalla()

    AA = str(input(' Ingrese EL Aminoacido  a Buscar: '))
    AA_final = AA.capitalize()
    print()
    time.sleep(1)
    print(' Cargando Base de Datos...')
    time.sleep(1.5)

    if AA_final in Aminoacidos:
        valor_AA = Aminoacidos.get(AA_final)
        print()
        print(' Aminoacido: ', str(AA_final) + ' Formula Molecular: ', valor_AA)
        time.sleep(1.5)
        print()
        enter = str(input(' Aprete Enter Para Continuar: '))
        if ( enter == ''):
            pass

    else:
        print(' Error de Busqueda...')
        time.sleep(2)

    BorrarPantalla()

def main_diccionario(Aminoacidos):
    time.sleep(1)
    BorrarPantalla()
    print(' Extayendo Datos....')
    time.sleep(1.5)
    BorrarPantalla()
    a = json.dumps(Aminoacidos, sort_keys = True) #Metodo dado para ordenar el Diccionario mediante Json
    b = json.loads(a)

    for key,value in b.items(): #Ciclo para mostrar AA + Formula
        print(' Nombre Aminoacido: {0} -- Formula Molecular: {1}'.format(key,value))

def exit_menu():
    pass

def menu_aminoacidos():
    Aminoacidos = {}
    while True:
        print('     Seleccione una de las siguientes opciones ')
        print()
        print(' --> A; Para Mostrar Aminoacidos en el Diccionario ')
        print(' --> B; Para Buscar un Aminoacido ')
        print(' --> I; Para Insertar un Aminoacido ')
        print(' --> E; Para Editar un Aminoacido ')
        print(' --> D; Para Eliminar un Aminoacido ')
        print(' --> M; Para Mostar Los Aminoacidos y Salir del Menu')
        print(' --> S; Para Salir del Menu ')
        print()
        opcion_ingresada = str(input('     Ingrese una Opcion del Menu: '))

        if (opcion_ingresada.upper() == 'B'):
            buscar_aminoacido(Aminoacidos)

        elif (opcion_ingresada.upper() == 'I'):
            Aminoacidos = insertar_aminoacido(Aminoacidos)

        elif (opcion_ingresada.upper() == 'E'):
            Aminoacidos = editar_aminoacido(Aminoacidos)

        elif (opcion_ingresada.upper() == 'D'):
            Aminoacidos = eliminar_aminoacido(Aminoacidos)

        elif(opcion_ingresada.upper() == 'A'):
            time.sleep(2)
            print(' --> Extrayendo Datos....')
            time.sleep(2)
            print(' --> Resultado Identico Encontrado ')
            time.sleep(2)
            BorrarPantalla()
            for k, v in Aminoacidos.items():
                print (' --> Nombre de Aminoacido: {0} -- Formula: {1} '.format(k,v))
            print()
            tecla = str(input(' --> Presione Cualquier Tecla Para Continuar: '))
            if ( tecla == ''):
                pass
            BorrarPantalla()

        elif (opcion_ingresada.upper() == 'M'):
            main_diccionario(Aminoacidos)
            break

        elif (opcion_ingresada.upper() == 'S'):
            print(' Vuelva Pronto')
            break

        else:
            time.sleep(1.5)
            print(' La opcion ingresada no es valida, intente nuevamente ')
            time.sleep(2.5)
            BorrarPantalla()
            pass

if __name__ == '__main__':
    menu_aminoacidos()
    cargar()
