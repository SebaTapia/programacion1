#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import time

def clear_screen(): #Funcion dada por el Profesor para borrar Pantalla.

    os.system('cls' if os.name =='nt' else 'clear')

def funcion_list_string(variable_string): #Funcion de transformacion de lista a str.

    formato_str = "  "
    variable_nueva = formato_str.join(variable_string) #Metodo para transformar una lista a str.

    return variable_nueva

def funcion_transformador(string,cantidad):

    #Creacion de nuestra lista principal.
    abecedario = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
                 'P','Q','R','S','T','U','V','W','X','Y','Z']

    new_string = []

    for x in range(len(string)): #Recorre el largo de la lista nueva.
        for y in range(len(abecedario)): #Recorre el largo de nuestro abecedario.
            if (abecedario[y] == string[x]):
                #A la lista se agregara agrega la posicion nueva
                #Posicion + desplazamiento = Nueva Letra
                #Usamos Modulo para evitar salirse del rango
                new_string.append(abecedario[(y + cantidad) % len(abecedario)])

    variable_nueva = funcion_list_string(new_string) #LLamado de la funcion que transforma listas en strings.
    print(' Mensaje Codificado: ', variable_nueva)


def menu(): #Funcion que genera el Menu

    while True:

        print(' Bienvenido ')
        print(' C: Para Codificar Mensaje ')
        print(' S: Para Salir ')
        print('\n')
        opcion = str(input(' Ingrese Una Opcion: '))

        if (opcion.upper() == 'C'):
            mensaje = str(input(' Ingrese un Mensaje: '))

            try:
                numero = int(input(' Ingrese Un Numero Para Desplazar: '))

                try:

                    print(' Mensaje Original: ', mensaje)
                    funcion_transformador(list(mensaje.upper()),numero)

                    press_enter = str(input(' Aprete Enter Para Avanzar: '))

                    if (press_enter == ''):
                        clear_screen()
                        pass

                except:
                    print(' Mensaje A Codificar Erroneo. Verificar Mensaje o Desplazamiento...Intente Nuevamente')
                    time.sleep(3)
                    clear_screen()

            except:

                print(' Mensaje A Codificar Erroneo. Verificar Mensaje o Desplazamiento...Intente Nuevamente')
                time.sleep(3)
                clear_screen()

        elif (opcion.upper() == 'S'):
            quit()

        else:
            print(' Opcion Invalida... ')
            time.sleep(3)
            clear_screen()


if __name__ == '__main__':
    menu()
