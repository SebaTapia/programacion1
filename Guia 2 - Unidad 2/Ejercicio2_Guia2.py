#!/usr/bin/env python3
# -*- coding:utf-8 -*-

def cargar_archivo():

    with open("covid_19_data.csv") as file:
        linea = file.readline()

        while linea != '':
            linea = linea.split(",")
            print(linea)
            linea = file.readline()
    return linea


cargar_archivo()
